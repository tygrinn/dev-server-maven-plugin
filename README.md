# Dev Server Maven Plugin

This plugin creates the 'dev-server' maven lifecyle which you
can attach plugin executions to.

## Usage

Install the plugin as a git submodule within your project:\
`git submodule add https://gitlab.com/tygrinn/dev-server-maven-plugin.git`

Add the module to your top level pom.xml

```$xml
<modules>
    <module>dev-server-maven-plugin</module>
</modules>
```

Add the plugin to your pom.xml:

```$xml
<plugin>
    <groupId>info.tygr.devservermavenplugin</groupId>
    <artifactId>dev-server-maven-plugin</artifactId>
    <version>1.0-SNAPSHOT</version>
    <extensions>true</extensions>
</plugin>
```

Set the phase of any plugin's execution to 'dev-server'

```$xml
<execution>
    <id>yarn-start</id>
    <goals>
        <goal>yarn</goal>
    </goals>
    <configuration>
        <arguments>start</arguments>
    </configuration>

    <!--suppress MavenModelInspection -->
    <phase>dev-server</phase>
</execution>
```

Start the development server\
`mvn dev-server`
